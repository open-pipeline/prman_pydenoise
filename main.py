#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sys

from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtGui import QIcon, QDesktopServices
from PyQt5.QtWidgets import QMainWindow, QApplication, QDesktopWidget, QAction

from denoiseui import DenoiseApp
from preferenceui import PreferenceApp


class DenoiseWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.init_ui()

    def init_ui(self):
        # Menu Bar

        self.menu = self.menuBar()

        self.reset_action = QAction('Reset')

        self.pref_action = QAction('Preferences')
        self.pref_action.setShortcut(Qt.ControlModifier + Qt.Key_P)
        self.pref_action.triggered.connect(self.launch_prefs)

        self.exit_action = QAction('Quit', self)
        self.exit_action.setShortcut(Qt.ControlModifier + Qt.Key_Q)
        self.exit_action.triggered.connect(self.quit_denoise)

        self.fileMenu = self.menu.addMenu('File')
        self.fileMenu.addAction(self.reset_action)
        self.fileMenu.addAction(self.pref_action)
        self.fileMenu.addAction(self.exit_action)

        self.help_action = QAction('Help')
        self.help_action.setShortcut(Qt.Key_F1)
        self.help_action.triggered.connect(self.launch_help)

        self.about_action = QAction('About')

        self.helpMenu = self.menu.addMenu('Help')
        self.helpMenu.addAction(self.help_action)
        self.helpMenu.addAction(self.about_action)

        # Create Window
        self.setFixedSize(560,625)
        self.center_frame()

        self.setWindowTitle('RenderMan PyDenoise')
        self.setWindowIcon(QIcon('renderman.ico'))

        self.setCentralWidget(DenoiseApp())

    def center_frame(self):
        self.frame_loc = self.frameGeometry()
        self.center_point = QDesktopWidget().availableGeometry().center()
        self.frame_loc.moveCenter(self.center_point)
        self.move(self.frame_loc.topLeft())

    def quit_denoise(self):
        self.close()

    def launch_prefs(self):
        self.prefs = PreferenceApp()

        self.pref_loc = self.prefs.frameGeometry()
        self.center_pref = self.geometry().center()
        self.pref_loc.moveCenter(self.center_pref)
        self.prefs.move(self.pref_loc.topLeft())
        self.prefs.show()

    def launch_help(self):
        self.wiki = ('https://gitlab.com/Open-Pipeline/'
                     'prman_pydenoise/wikis/home')
        QDesktopServices.openUrl(QUrl(self.wiki))


if __name__ == '__main__':

    app = QApplication(sys.argv)
    gui = DenoiseWindow()
    gui.show()
    sys.exit(app.exec_())
