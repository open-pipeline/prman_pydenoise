#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import os
import platform

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QVBoxLayout, QHBoxLayout, QGroupBox,
                             QLineEdit, QPushButton, QListWidget, QCheckBox,
                             QRadioButton, QPlainTextEdit, QSpinBox, QLabel,
                             QProgressBar, QFileDialog)


class DenoiseApp(QWidget):
    def __init__(self):
        super().__init__()
        self.MAX_THREADS = os.cpu_count()
        self.OS = platform.system()
        self.app_init_ui()

    def app_init_ui(self):
        # Init Main Layouts
        self.base_layout = QVBoxLayout()
        self.main_split = QHBoxLayout()
        self.options = QVBoxLayout()
        self.lister = QVBoxLayout()
        self.console = QVBoxLayout()
        self.actions = QHBoxLayout()
        self.setLayout(self.base_layout)

        self.main_split.addLayout(self.options)
        self.main_split.addLayout(self.lister)
        self.base_layout.addLayout(self.main_split)
        self.base_layout.addLayout(self.console)
        self.base_layout.addLayout(self.actions)

        # Output Frame
        self.output_group = QGroupBox('Output Options')

        self.out_name_label = QLabel('Output Name')
        self.out_name_label.setMinimumWidth(100)
        self.out_name_label.setMaximumWidth(100)
        self.out_name_line = QLineEdit()
        self.out_name_line.setPlaceholderText('Optional')

        self.out_name_layout = QHBoxLayout()
        self.out_name_layout.addWidget(self.out_name_label)
        self.out_name_layout.addWidget(self.out_name_line)

        self.out_dir_label = QLabel('Output Folder')
        self.out_dir_label.setMinimumWidth(100)
        self.out_dir_label.setMaximumWidth(100)
        self.out_dir_line = QLineEdit()
        self.out_dir_line.setPlaceholderText('Optional')
        self.out_dir_btn = QPushButton('...')
        self.out_dir_btn.setMaximumWidth(35)
        self.out_dir_btn.clicked.connect(lambda: self.directory_browse(self.out_dir_line))

        self.out_dir_layout = QHBoxLayout()
        self.out_dir_layout.addWidget(self.out_dir_label)
        self.out_dir_layout.addWidget(self.out_dir_line)
        self.out_dir_layout.addWidget(self.out_dir_btn)

        self.output_group_layout = QVBoxLayout()
        self.output_group_layout.addLayout(self.out_name_layout)
        self.output_group_layout.addLayout(self.out_dir_layout)
        self.output_group.setLayout(self.output_group_layout)

        self.options.addWidget(self.output_group)

        # Denoise Type
        self.denoise_group = QGroupBox('Denoise Type')

        self.denoise_frame = QRadioButton('Frame')
        self.denoise_cross = QRadioButton('Cross-Frame')

        self.denoise_frame.setChecked(1)

        self.denoise_group_layout = QVBoxLayout()
        self.denoise_group_layout.addWidget(self.denoise_frame)
        self.denoise_group_layout.addWidget(self.denoise_cross)
        self.denoise_group.setLayout(self.denoise_group_layout)

        self.options.addWidget(self.denoise_group)

        # Denoise Flags
        self.vari_group = QGroupBox('Denoise Flags')

        self.vari_skip_first = QCheckBox('Skip First')
        self.vari_skip_last = QCheckBox('Skip Last')
        self.vari_gpu = QCheckBox('Enable GPU')
        self.vari_gpu.clicked.connect(self.gpu_threads_toggle)

        self.vari_threads_label = QLabel('Threads')
        self.vari_threads_value = QSpinBox()
        self.vari_threads_value.setRange(1, self.MAX_THREADS)
        self.vari_threads_value.setValue(self.MAX_THREADS - 1)

        self.vari_threads_layout = QHBoxLayout()
        self.vari_threads_layout.addWidget(self.vari_threads_label)
        self.vari_threads_layout.addWidget(self.vari_threads_value)
        self.vari_threads_layout.addStretch(1)

        self.vari_group_layout = QVBoxLayout()
        self.vari_group_layout.addWidget(self.vari_skip_first)
        self.vari_group_layout.addWidget(self.vari_skip_last)
        self.vari_group_layout.addWidget(self.vari_gpu)
        self.vari_group_layout.addLayout(self.vari_threads_layout)
        self.vari_group.setLayout(self.vari_group_layout)

        self.options.addWidget(self.vari_group)

        # File Lister
        self.list_area = QListWidget()

        self.list_add_file = QPushButton('Add')
        self.list_rem_file = QPushButton('Remove')
        self.list_rem_clear = QPushButton('Clear')

        self.list_btn_bar = QHBoxLayout()
        self.list_btn_bar.addWidget(self.list_add_file)
        self.list_btn_bar.addWidget(self.list_rem_file)
        self.list_btn_bar.addWidget(self.list_rem_clear)

        self.lister.addWidget(self.list_area)
        self.lister.addLayout(self.list_btn_bar)

        # Console Output
        self.console_terminal = QPlainTextEdit()
        self.console_terminal.setReadOnly(1)
        self.console_terminal.setContextMenuPolicy(Qt.PreventContextMenu)
        self.console_terminal.setTextInteractionFlags(Qt.NoTextInteraction)
        self.console_terminal.setMaximumHeight(130)

        self.console_progress = QProgressBar()
        self.console_progress.setTextVisible(0)
        self.console_progress.setMaximumHeight(6)

        self.console.addWidget(self.console_terminal)
        self.console.addWidget(self.console_progress)

        # Action Bar
        self.action_denoise = QPushButton('Denoise')
        self.action_cancel = QPushButton('Stop')

        self.actions.addStretch(1)
        self.actions.addWidget(self.action_denoise)
        self.actions.addWidget(self.action_cancel)

    def gpu_threads_toggle(self):
        if self.vari_gpu.isChecked():
            self.vari_threads_value.setEnabled(0)
        else:
            self.vari_threads_value.setEnabled(1)

    def directory_browse(self, edit_value):
        if len(edit_value.text()) > 0:
            tempdir = edit_value.text()
        elif self.OS == 'Windows':
            tempdir = os.environ['HOMEPATH']
        else:
            tempdir = os.environ['HOME']

        directory =\
            QFileDialog.getExistingDirectory(caption='PyDenoise Browser',
                                             directory=tempdir)

        if directory == '':
            pass
        else:
            edit_value.setText(directory)
